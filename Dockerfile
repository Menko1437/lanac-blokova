FROM rust:1.56.1
RUN USER=root cargo new --bin vachain
WORKDIR /vachain

COPY Cargo.lock Cargo.lock
COPY Cargo.toml Cargo.toml
COPY src src

# build for release
RUN cargo build --release

CMD ["./target/release/blockchain"]

