pub mod block;
pub mod chain_errors;

use serde::{Deserialize, Serialize};

use self::{block::Block, chain_errors::ChainError};

#[derive(Serialize, Deserialize)]
pub struct Blockchain {
    pub blocks: Vec<Block>,
}

impl Blockchain {
    pub fn new() -> Blockchain {
        let blocks: Vec<Block> = vec![Blockchain::genesis()];
        Self { blocks }
    }

    fn genesis() -> Block {
        Block::new(
            1,
            "d4e2d19a56ea30bf68b87cecaf3f0ad7b3059a7b53a178476b30fe5fc6db354e".to_string(),
            "".to_string(),
            1636411004u64,
            "GENESIS".as_bytes().to_vec(),
        )
    }

    pub fn generate_next_block(&mut self, data: Vec<u8>) -> Result<(), ChainError> {
        let prev_node = match self.last_block() {
            Some(node) => node,
            None => return Err(ChainError::new("Blockchain is empty")),
        };

        let now = chrono::Utc::now();

        let next = Block::new(
            prev_node.index + 1,
            Block::calc_hash(
                prev_node.index + 1,
                &prev_node.hash,
                now.timestamp() as u64,
                &data,
            ),
            prev_node.hash.clone(),
            now.timestamp() as u64,
            data,
        );

        self.blocks.push(next);
        Ok(())
    }

    pub fn validate_chain(&self) -> Result<(), ChainError> {
        if !self.blocks.is_empty() && self.blocks[0] != Blockchain::genesis() {
            return Err(ChainError::new("Invalid genesis node"));
        }

        for node in self.blocks.iter() {
            if !node.to_owned().check_node_validity(self.last_block()) {
                return Err(ChainError::new("Blockchain node failed validity test."));
            }
        }
        Ok(())
    }

    pub fn solve_conflict(&mut self, other: &Blockchain) {
        if other.validate_chain().is_ok() && other.blocks.len() > self.blocks.len() {
            self.blocks = other.blocks.clone()
        }
    }

    fn last_block(&self) -> Option<&Block> {
        self.blocks.last()
    }
}
