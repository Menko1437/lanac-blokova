use log::error;
use std::fmt;

#[derive(Debug, Clone)]
pub struct ChainError<'a> {
    message: &'a str,
}

impl<'a> ChainError<'a> {
    pub fn new(message: &'a str) -> Self {
        ChainError { message }
    }
}

impl<'a> fmt::Display for ChainError<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        error!("Node Error: {}", self.message);
        write!(f, "Node Error. {}", self.message)
    }
}
