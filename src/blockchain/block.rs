use crypto_hash;
use serde::{Deserialize, Serialize};

#[derive(Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct Block {
    pub index: u64,
    pub hash: String,
    pub prev_hash: String,
    pub timestamp: u64,
    pub data: Vec<u8>,
}

impl Block {
    pub fn new(index: u64, hash: String, prev_hash: String, timestamp: u64, data: Vec<u8>) -> Self {
        Self {
            index,
            hash,
            prev_hash,
            timestamp,
            data,
        }
    }

    pub fn calc_hash(index: u64, prev_hash: &str, timestamp: u64, data: &[u8]) -> String {
        let to_hash = format!(
            "{}{}{}{}",
            index,
            prev_hash,
            timestamp,
            data.iter().map(|byte| byte.to_string()).collect::<String>(),
        );

        println!("HASHING: {}", to_hash);

        return crypto_hash::hex_digest(crypto_hash::Algorithm::SHA256, to_hash.as_bytes());
    }

    pub fn check_node_validity(self, prev_block: Option<&Block>) -> bool {
        match prev_block {
            Some(prev_block) => {
                self.index == prev_block.index + 1
                    || self.prev_hash == prev_block.hash
                    || Block::calc_hash(self.index, &self.prev_hash, self.timestamp, &self.data)
                        == self.hash
            }
            None => false,
        }
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn hash() {
        let hash = super::Block::calc_hash(
            1,
            &"Hello".to_string(),
            1636407261u64,
            &"Hello blockchain!".as_bytes().to_vec(),
        );

        assert_eq!(
            hash,
            "d63a187bcfe16532ddccdf5e02bd2af5dae7bcdad9b2a0af9896e471ce0ae6bf".to_string()
        );
    }
}
