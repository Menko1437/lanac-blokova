use std::{collections::HashSet, error::Error, time::Duration};

use libp2p::{
    core::{
        muxing::StreamMuxerBox,
        transport::{upgrade, Boxed},
    },
    floodsub::{Floodsub, FloodsubEvent, Topic},
    identity::Keypair,
    mdns::{Mdns, MdnsConfig, MdnsEvent},
    mplex,
    noise::{self, NoiseConfig, X25519Spec},
    ping::{Ping, PingEvent, PingSuccess},
    swarm::NetworkBehaviourEventProcess,
    tcp::TokioTcpConfig,
    PeerId, Transport,
};

use log::info;
use p2p_node_stats::Stats;
use serde::{Deserialize, Serialize};
use tokio::sync::mpsc;

use crate::util;

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct PendingTxMessage {
    pub sent_time_millis: u128,
    pub data: Vec<u8>,
}

const PENDING_TX_TOPIC: &str = "pending_tx";

#[derive(libp2p::NetworkBehaviour)]
pub struct VAChainNetworkBehaviour {
    pub floodsub: Floodsub,
    pub mdns: Mdns,
    pub ping: Ping,

    #[behaviour(ignore)]
    pub stats: Stats,
}

impl VAChainNetworkBehaviour {
    pub async fn new(
        peer_id: PeerId,
        window_size: usize,
    ) -> Result<Self, Box<dyn std::error::Error>> {
        let mdns = Mdns::new(MdnsConfig::default()).await?;
        let ping = Ping::default();
        Ok(VAChainNetworkBehaviour {
            floodsub: Floodsub::new(peer_id),
            mdns,
            ping,
            stats: Stats::new(window_size, peer_id.to_string()),
        })
    }
}

impl NetworkBehaviourEventProcess<PingEvent> for VAChainNetworkBehaviour {
    fn inject_event(&mut self, message: PingEvent) {
        if let Result::Ok(PingSuccess::Ping { rtt }) = message.result {
            let peer_id = message.peer;
            info!("Ping {:?} {:?}", peer_id.clone(), rtt);
            self.stats.add_ping(peer_id.to_string(), rtt)
        }
    }
}

impl NetworkBehaviourEventProcess<FloodsubEvent> for VAChainNetworkBehaviour {
    fn inject_event(&mut self, event: FloodsubEvent) {
        match event {
            FloodsubEvent::Message(message) => {
                if let [topic] = &message.topics[..] {
                    match &String::from(topic.clone())[..] {
                        PENDING_TX_TOPIC => {
                            let message_data: PendingTxMessage =
                                bincode::deserialize(&message.data[..]).unwrap();
                            let elapsed_time_millis =
                                util::current_time_millis() - message_data.sent_time_millis;
                            info!(
                                "Received pending tx {:?} bytes from {:?} in {:?} ms",
                                message.data.len(),
                                message.source,
                                elapsed_time_millis
                            );

                            let peer_id = message.source;
                            self.stats.add_transmission(
                                peer_id.to_string(),
                                Duration::from_millis(elapsed_time_millis as u64),
                                message.data.len() as u32,
                            );
                        }
                        _ => info!("Unsupported topic {:?}", topic),
                    }
                }
            }
            FloodsubEvent::Subscribed { peer_id, topic } => {
                info!(
                    "Peer: {} subscribed on {} topic",
                    peer_id,
                    topic.id().to_string()
                );
            }
            FloodsubEvent::Unsubscribed { peer_id, topic } => {
                info!(
                    "Peer: {} unsubscribed on {} topic",
                    peer_id,
                    topic.id().to_string()
                );
            }
        }
    }
}

// Automaticly finds new peers that connect to network and adds them to floodsub
impl NetworkBehaviourEventProcess<MdnsEvent> for VAChainNetworkBehaviour {
    fn inject_event(&mut self, event: MdnsEvent) {
        match event {
            MdnsEvent::Discovered(list) => {
                for (peer, _) in list {
                    self.floodsub.add_node_to_partial_view(peer);
                }
            }
            MdnsEvent::Expired(list) => {
                for (peer, _) in list {
                    if !self.mdns.has_node(&peer) {
                        self.floodsub.remove_node_from_partial_view(&peer);
                    }
                }
            }
        }
    }
}

pub struct Network {
    pub keys: Keypair,
    pub peer_id: PeerId,
    pub topics: HashSet<Topic>,
    pub transport: Option<Boxed<(PeerId, StreamMuxerBox)>>,
    pub behaviour: VAChainNetworkBehaviour,
}

impl Network {
    pub async fn new() -> Result<Network, Box<dyn Error>> {
        let keys = Keypair::generate_ed25519();
        let pub_key = keys.public();
        Ok(Self {
            keys,
            peer_id: PeerId::from_public_key(pub_key.clone()),
            topics: HashSet::new(),
            transport: None,
            behaviour: VAChainNetworkBehaviour::new(PeerId::from_public_key(pub_key), 512).await?,
        })
    }

    pub fn register_topic(&mut self, topic: String) -> bool {
        self.topics.insert(Topic::new(topic))
    }

    pub fn unregister_topic(&mut self, topic: String) -> bool {
        self.topics.remove(&Topic::new(topic))
    }

    pub fn create_auth_keys(&self) -> noise::AuthenticKeypair<X25519Spec> {
        noise::Keypair::<X25519Spec>::new()
            .into_authentic(&self.keys)
            .expect("Signing keypair failed")
    }

    pub fn create_sender_reciever_pair<T>(
        &self,
    ) -> (mpsc::UnboundedSender<T>, mpsc::UnboundedReceiver<T>) {
        mpsc::unbounded_channel::<T>()
    }

    pub fn configurate_transport(&mut self, auth_keypair: noise::AuthenticKeypair<X25519Spec>) {
        self.transport = Some(
            TokioTcpConfig::new()
                .upgrade(upgrade::Version::V1)
                .authenticate(NoiseConfig::xx(auth_keypair).into_authenticated())
                .multiplex(mplex::MplexConfig::new())
                .boxed(),
        );
    }
}
