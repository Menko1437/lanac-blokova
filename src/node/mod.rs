pub mod network;

use libp2p::{
    core::{connection::ListenerId, muxing::StreamMuxerBox, transport::Boxed},
    floodsub::Topic,
    futures::{executor::block_on, future, StreamExt},
    swarm::{SwarmBuilder, SwarmEvent},
    PeerId, Swarm,
};

use crate::blockchain::Blockchain;

use std::{
    sync::{Arc, Mutex},
    task::Poll,
};

use network::VAChainNetworkBehaviour;

use log::info;

pub struct Node {
    pub blockchain: Arc<Mutex<Blockchain>>,
    pub swarm: Swarm<VAChainNetworkBehaviour>,
}

impl Node {
    pub fn new(
        peer: PeerId,
        transport: Boxed<(PeerId, StreamMuxerBox)>,
        behaviour: VAChainNetworkBehaviour,
    ) -> Node {
        Self {
            blockchain: Arc::new(Mutex::new(Blockchain::new())),
            swarm: SwarmBuilder::new(transport, behaviour, peer)
                .executor(Box::new(|fut| {
                    tokio::spawn(fut);
                }))
                .build(),
        }
    }

    pub fn listen_on(&mut self, addr: &str) -> ListenerId {
        self.swarm.listen_on(addr.parse().unwrap()).unwrap()
    }

    pub fn poll_addr(&mut self) {
        for addr in Swarm::listeners(&self.swarm) {
            info!("Listening on: {:?}", addr)
        }
    }

    pub fn start(&mut self) {
        block_on(future::poll_fn(move |cx| loop {
            match self.swarm.poll_next_unpin(cx) {
                Poll::Ready(Some(event)) => match event {
                    SwarmEvent::NewListenAddr { address, .. } => {
                        println!("Listening on {:?}", address)
                    }
                    SwarmEvent::Behaviour(event) => println!("Event {:?}", event),
                    _ => {}
                },
                Poll::Ready(None) => return Poll::Ready(()),
                Poll::Pending => return Poll::Pending,
            }
        }));
    }

    pub fn send_data(&mut self, topic: &str, message: &str) {
        let topic = Topic::new(topic);
        let message = bincode::serialize(message);

        self.swarm
            .behaviour_mut()
            .floodsub
            .publish(topic, message.unwrap());
    }
}
