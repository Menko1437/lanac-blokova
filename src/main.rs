// Remember to remove this later
#![allow(dead_code)]
#![allow(unused_variables)]

use node::{network::Network, Node};

mod blockchain;
mod log;
mod node;
mod util;

const NODE_ADDRESS: &str = "/ip4/0.0.0.0/tcp/54381";
const TEST_ADDRESS: &str = "/ip4/10.144.13.50/tcp/54381";

#[tokio::main]
async fn main() {
    log::setup_logger();

    let mut network = Network::new().await.unwrap();

    let peer = network.peer_id;
    let auth_keys = network.create_auth_keys();
    network.configurate_transport(auth_keys);
    let behaviour = network.behaviour;
    let transport = network.transport.unwrap();

    let mut node = Node::new(peer, transport, behaviour);

    let listener = node.listen_on(NODE_ADDRESS);

    node.start();
}
